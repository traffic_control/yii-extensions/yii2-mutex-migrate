<?php

namespace tests\unit;

use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use credy\yii2mutexmigrate\MigrateController;
use yii\base\Module;
use yii\console\ExitCode;
use yii\console\Request;
use yii\console\Response;
use yii\mutex\FileMutex;

class MigrateControllerTest extends Unit
{
    public function testMutexSaysNo()
    {
        $mutex = $this->makeEmpty(FileMutex::class, [
            'acquire' => false
        ]);

        $controller = $this->construct(MigrateController::class, [
            'test',
            new Module('test'),
            [
                'request' => new Request(),
                'response' => new Response(),
                'mutex' => $mutex,
            ]
        ], [
            'stderr' => Expected::once(function ($error) {
                $this->assertStringContainsString('Cannot acquire mutex, is migration happening somewhere else', $error);
            })
        ]);

        $this->assertEquals(ExitCode::TEMPFAIL, $controller->actionUp());
    }

    public function testMutexSaysYes()
    {
        $mutexKey = null;
        $mutex = $this->makeEmpty(FileMutex::class, [
            'acquire' => Expected::once(function ($key) use (&$mutexKey) {
                $mutexKey = $key;
                return true;
            }),
            'release' => Expected::once(function ($key) use (&$mutexKey) {
                $this->assertEquals($mutexKey, $key);
            }),
        ]);

        $controller = $this->construct(MigrateController::class, [
            'test',
            new Module('test'),
            [
                'request' => new Request(),
                'response' => new Response(),
                'mutex' => $mutex,
            ]
        ], [
            'getNewMigrations' => Expected::once(function () {
                return [];
            }),
            'stdout' => function () {
            }
        ]);

        $this->assertEquals(ExitCode::OK, $controller->actionUp());
    }
}
