<?php

namespace credy\yii2mutexmigrate;

use yii\console\ExitCode;
use yii\di\Instance;
use yii\mutex\Mutex;

class MigrateController extends \yii\console\controllers\MigrateController
{
    /**
     * @var Mutex|string|array
     */
    public $mutex = 'mutex';

    public function actionUp($limit = 0)
    {
        /**
         * @var Mutex
         */
        $this->mutex = Instance::ensure($this->mutex, Mutex::class);

        if (!$this->mutex->acquire(self::class)) {
            $this->stderr('Cannot acquire mutex, is migration happening somewhere else?' . PHP_EOL);
            return ExitCode::TEMPFAIL;
        }

        $result = parent::actionUp($limit);
        $this->mutex->release(self::class);

        return $result;
    }
}
